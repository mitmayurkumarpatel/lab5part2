/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Lab5Part2;

/**
 *
 * @author mitpa
 */
public class StudentController 
{
   private StudentView view;
   private Student model;
   
   public StudentController(StudentView view,Student model)
   {
       this.view = view;
       this.model = model;
   }

    StudentController(Student model, StudentView view) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
   public void setStudentName(String name)
   {
       model.setName(name);
   }
   public String getStudentName()
   {
       return model.getName();
   }
   public String getStudentRollNo()
           
   {
        return model.getRollNo();
   }
   public void updateView()
   {
       view.printStudentDetails(model.getName() , model.getRollNo());
   }
    
}
