/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Lab5Part2;

/**
 *
 * @author mitpa
 */
public class MVCPatternDemo
{
    public static void main(String[]args)
    {
        Student model = retriveStudentFromDatabase();
        
        StudentView view = new StudentView();
        
        StudentController controller = new StudentController(model,view);
        
        controller.updateView();
        controller.setStudentName("Mit");
        
        controller.updateView();
    }
    private static Student retriveStudentFromDatabase()
    {
        Student student = new Student();
        student.setName("hello");
        student.setRollNo("1");
        return student;
    }
}
